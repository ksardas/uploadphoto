package com.example.uploadphoto;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class GreyscaleFilter extends Activity implements android.view.View.OnClickListener {

	private Button filter;
	private Button WithoutFilter;
	private Button send;
	private Button save;
	private ImageView image;
	private Bitmap photo;
	private String fName;
	private Context mContext;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.filter);
	
		
		Intent i = getIntent();
	    fName = i.getStringExtra("fname");    
	    
	    if(fName != null) photo = BitmapFactory.decodeFile(fName);
	    else photo = MyPhoto.getBitmap();
	    
	    
		
		filter = (Button)findViewById(R.id.filter);
		WithoutFilter = (Button)findViewById(R.id.without_filter);
		image = (TouchImageView)findViewById(R.id.im);
		send = (Button)findViewById(R.id.send);
		save = (Button)findViewById(R.id.save);
		mContext = this;
		
		
		filter.setOnClickListener(this);
		WithoutFilter.setOnClickListener(this);
		send.setOnClickListener(this);
		save.setOnClickListener(this);
		
		
		
	}
	
	
	
	@Override
	protected void onResume() {
		
		image.setImageBitmap(photo);
		super.onResume();
	}



	public Bitmap toGrayscale(Bitmap bmpOriginal)
	{        
	    int width, height;
	    height = bmpOriginal.getHeight();
	    width = bmpOriginal.getWidth();    

	    Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
	    Canvas c = new Canvas(bmpGrayscale);
	    Paint paint = new Paint();
	    ColorMatrix cm = new ColorMatrix();
	    cm.setSaturation(0);
	    ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
	    paint.setColorFilter(f);
	    c.drawBitmap(bmpOriginal, 0, 0, paint);
	    return bmpGrayscale;
	}

	@Override
	public void onClick(View v) {
		
		if(v.getId() == R.id.filter){
			
			
			Filter filt = new Filter();
			filt.execute();
		}
		
		if(v.getId() == R.id.without_filter){
			
			image.setImageBitmap(photo);
			
		}
		
		if(v.getId() == R.id.send){
			
			Intent intent = new Intent(this, DBRoulette.class);
            startActivity(intent);
			
		}
		
		if(v.getId() == R.id.save){
			
			
			SaveImage i = new SaveImage();
			i.execute();
			
			
		}
		
		
		
	}
	
	
	
	
	
	private File generateFile() {

		  if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
		    return null;

		  File path = new File (Environment.getExternalStorageDirectory(), "UploadPhoto");
		  if (! path.exists()){
		    if (! path.mkdirs()){
		      return null;
		    }
		  }
		            
		  String timeStamp = String.valueOf(System.currentTimeMillis());
		  File newFile = new File(path.getPath() + File.separator + timeStamp + ".jpg");
		  return newFile;
	}
	
	
	class Filter extends AsyncTask<Void, Void, Void> {

		private Bitmap ph;
	    
		@Override
	    protected Void doInBackground(Void... params) {
	    	
			Bitmap p = photo;
	        ph = toGrayscale(p);
			return null;
	      
	    }

	    @Override
	    protected void onPostExecute(Void result) {
	    	
	    	MyPhoto.setBitmap(ph);
	    	image.setImageBitmap(ph);
	      
	    }
	  }
	
	
	
	class SaveImage extends AsyncTask<Void, Void, Void> {
		
		private File si;
		
		private SaveImage(){
			
			si = generateFile();
		}
	    
		@Override
	    protected Void doInBackground(Void... params) {
	    	
			FileOutputStream out = null;
			try {
			    out = new FileOutputStream(si);
			    MyPhoto.getBitmap().compress(Bitmap.CompressFormat.JPEG, 90, out);
			} catch (Exception e) {
			    e.printStackTrace();
			} finally {
			    try {
			        if (out != null) {
			            out.close();
			      }
			        
			    } 
			    
			    catch (IOException e) {
			    	
			        e.printStackTrace();
			    }
			}
			
			
			addImageToGallery(si.getPath(), mContext);

			return null;
	      
	    }
		
		
		@Override
	    protected void onPostExecute(Void result) {
	    	
			Toast toast = Toast.makeText(mContext, "Image successfully saved: " + si.getPath(), Toast.LENGTH_LONG);
			toast.show();
	      
	    }

	 
	  }
	
	
	public static void addImageToGallery(final String filePath, final Context context) {

	    ContentValues values = new ContentValues();

	    values.put(Images.Media.DATE_TAKEN, System.currentTimeMillis());
	    values.put(Images.Media.MIME_TYPE, "image/jpeg");
	    values.put(MediaStore.MediaColumns.DATA, filePath);

	    context.getContentResolver().insert(Images.Media.EXTERNAL_CONTENT_URI, values);
	}

	



	
}
