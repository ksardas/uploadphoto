package com.example.uploadphoto;


import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;


public class MainActivity extends Activity implements OnClickListener {
	
	private Context mContext;
	private ImageButton photo;
	private ImageButton gallery;
	
	private static Uri fileUri;
	private static final int PHOTO_INTENT_REQUEST_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        mContext = this;
        
        photo = (ImageButton)findViewById(R.id.Photo);
        photo.setOnClickListener(this);
        gallery = (ImageButton)findViewById(R.id.Gallery);
        gallery.setOnClickListener(this);
        
    }

   
    
	@Override
	public void onClick(View v) {
		
		if(v.getId() == R.id.Photo)
		{
			Intent intent = new Intent( MediaStore.ACTION_IMAGE_CAPTURE );
		    fileUri = getOutputMediaFileUri();
		    Log.d("MY", fileUri.getPath());
			intent.putExtra( MediaStore.EXTRA_OUTPUT, fileUri );
			 startActivityForResult(intent, PHOTO_INTENT_REQUEST_CODE);
		}
		
		
		if(v.getId() == R.id.Gallery)
		{
			Intent intent = new Intent();
	        intent.setType("image/*");
	        intent.putExtra("return-data", true);
	        intent.setAction(Intent.ACTION_GET_CONTENT);
	        startActivityForResult(Intent.createChooser(intent,"Select Picture"), 1);
		}
		
		
		
	}
	

	
	public static Bitmap RotateBitmap(Bitmap source, float angle)
	{
	      Matrix matrix = new Matrix();
	      matrix.postRotate(angle);
	      return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		if(resultCode != Activity.RESULT_CANCELED){
			if (requestCode == PHOTO_INTENT_REQUEST_CODE) {
				
				Bitmap thumbnail = BitmapFactory.decodeFile(fileUri.getPath());
				
			
				
				ExifInterface exif = null;
				try {
					exif = new  ExifInterface ( fileUri.getPath());
				} catch (IOException e) {
				  e.printStackTrace();
				} 
		        String orientString = exif . getAttribute ( ExifInterface . TAG_ORIENTATION ); 
		        int orientation = orientString !=  null  ?  Integer . parseInt ( orientString )  :  ExifInterface . ORIENTATION_NORMAL ;
		            
		        int rotationAngle =  0 ; 
		        if  ( orientation ==  ExifInterface . ORIENTATION_ROTATE_90 ) rotationAngle =  90 ; 
		        if  ( orientation ==  ExifInterface . ORIENTATION_ROTATE_180 ) rotationAngle =  180 ; 
		        if  ( orientation ==  ExifInterface . ORIENTATION_ROTATE_270 ) rotationAngle =  270 ;
		        
		        MyPhoto.setBitmap(RotateBitmap(thumbnail, rotationAngle));
	        
				Intent i = new Intent(mContext, GreyscaleFilter.class);
				startActivity (i);
			}
		}	
		
		
			
		
		
		
		 if (requestCode == 1 && resultCode == RESULT_OK && null != data) {
	            Uri selectedImage = data.getData();
	            String[] filePathColumn = { MediaStore.Images.Media.DATA };
	 
	            Cursor cursor = getContentResolver().query(selectedImage,
	                    filePathColumn, null, null, null);
	            cursor.moveToFirst();
	 
	            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
	            String picturePath = cursor.getString(columnIndex);
	            cursor.close();
	             
	            Bitmap bitmap = BitmapFactory.decodeFile(picturePath);
	            
	            
	            
	            
	            MyPhoto.setBitmap(bitmap);
	            Intent intent = new Intent(this, GreyscaleFilter.class);
	            startActivity(intent);
	         
	        }
		
		 super.onActivityResult(requestCode, resultCode, data);
		
        
	}
	
	
	
	public Bitmap decodeFile(String path) {
        try {
        	
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, o);
            
            // The new size we want to scale to
            final int REQUIRED_SIZE = 70;

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_SIZE && o.outHeight / scale / 2 >= REQUIRED_SIZE)
                scale *= 2;

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeFile(path, o2);
        } catch (Throwable e) {
            e.printStackTrace();
        }
        return null;

    }
	
	/** Create a file Uri for saving an image*/
	private static Uri getOutputMediaFileUri(){
	  return Uri.fromFile(getOutputMediaFile());
	}
	
	/** Create a File for saving an image */
	private static File getOutputMediaFile(){
	    // To be safe, you should check that the SDCard is mounted
	    // using Environment.getExternalStorageState() before doing this.

	    File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
	          Environment.DIRECTORY_PICTURES), "MyCameraApp");
	    // This location works best if you want the created images to be shared
	    // between applications and persist after your app has been uninstalled.

	    // Create the storage directory if it does not exist
	    if (! mediaStorageDir.exists()){
	        if (! mediaStorageDir.mkdirs()){
	            Log.d("MyCameraApp", "failed to create directory");
	            return null;
	        }
	    }

	    // Create a media file name
	    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
	    File mediaFile = new File(mediaStorageDir.getPath() + File.separator +
	        "IMG_"+ timeStamp + ".jpg");

	    return mediaFile;
	}
	
	


    
}
