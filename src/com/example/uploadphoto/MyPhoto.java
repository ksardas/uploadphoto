package com.example.uploadphoto;

import java.io.File;

import android.graphics.Bitmap;


public final class MyPhoto {
	
	private static Bitmap b = null;
	
	public static void setBitmap(Bitmap bit){
		
		b = bit;
		
	}
	
	public static Bitmap getBitmap(){
		
		return b;
		
	}

}
